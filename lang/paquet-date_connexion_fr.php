<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'date_connexion_description' => 'Ajoute 3 dates (automatiques) pour les auteurs :
- date_connexion : date de dernière connexion de l’auteur
- date_connexion_precedente : date de la pénultième connexion de l’auteur
- date_suivi_activite : date de précédente activité de l’auteur, c\'est à dire soit date_connexion_precedente, soit définie manuellement',
	'date_connexion_nom' => 'Date de connexion',
	'date_connexion_slogan' => 'Pouvoir utiliser des dates de connexion des auteurs',
);
